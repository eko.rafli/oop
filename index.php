<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');
$sheep = new Animal('Shaun');
echo "Nama hewan : " .$sheep->name. "<br>";
echo "Jumlah kaki : " .$sheep->legs. "<br>";
echo "Berdarah dingin? " .$sheep->cold_blooded. "<br><br>";

$kodok = new Frog('Budug');
echo "Nama hewan : " .$kodok->name. "<br>";
echo "Jumlah kaki : " .$kodok->legs. "<br>";
echo "Berdarah dingin? " .$kodok->cold_blooded. "<br>";
echo "Lompat! ";
echo $kodok->jump(). "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Nama hewan : " .$sungokong->name. "<br>";
echo "Jumlah kaki : " .$sungokong->legs. "<br>";
echo "Berdarah dingin? " .$sungokong->cold_blooded. "<br>";
echo "Yell! ";
echo $sungokong->yell(). "<br>";

?>